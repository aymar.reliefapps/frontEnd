import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Video } from 'src/app/classes/video';
import { VideoService } from 'src/app/services/video.service';

@Component({
  selector: 'History',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnChanges, OnInit {

  videos: Video[];
  selectedVideo: Video; 
  
  constructor(private videoService: VideoService) {}

  ngOnChanges(){
  }
  
  ngOnInit() {
    this.getVideos();
  }

  getVideos(): void {
    this.videoService.getVideos()
        .subscribe(videos => this.videos = videos);
  }

  add(link: string): void {
    link = link.trim();
    if (!link) { return; }
    this.videoService.addVideo({ link } as Video)
      .subscribe(video => {
        this.videos.push(video);
      });
  }

  delete(video: Video): void {
    this.videos = this.videos.filter(h => h !== video);
    this.videoService.deleteVideo(video).subscribe();
  }

  public playVideo(video: Video): void {
    this.videoService.setLink(video.link);
  }

  /*
  @Input()
  public video: Video;

  constructor(private videoService: VideoService) { }

  ngOnInit(): void {
  }

  // Play a video which is store in the history
  public removeVideo(): void {
    this.videoService.removeVideo(this.video.id);
  }

  // Remove a video from the history 
  public playVideo(): void {
    this.videoService.setLink(this.video.link);
  }
*/
}
