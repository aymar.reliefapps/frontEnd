import { Component, OnInit, Input } from '@angular/core';
import { VideoService } from 'src/app/services/video.service'
import { Video } from 'src/app/classes/video';

@Component({
  selector: 'searchBar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Input() videos: Video[];

  selectedVideo: Video;

  @Input() public linkis: string;  public videoLink: string

  constructor(private videoService: VideoService) { }

  ngOnInit() {
    this.getVideos();
  }

  getVideos(): void {
    this.videoService.getVideos()
        .subscribe(videos => this.videos = videos);
  }

  add(link:string): void { 

    this.videoLink = this.videoLink.trim();
    this.linkis = this.videoLink;
    link = this.videoLink;

    if (!this.videoLink) { return; }
    this.videoService.addVideo({ link } as Video)
      .subscribe(video => {
        this.videos.push(video);
      });

      this.videoLink = "";
  }

  delete(video: Video): void {
    this.videos = this.videos.filter(h => h !== video);
    this.videoService.deleteVideo(video).subscribe();
  }

  /*
  public isReadable: boolean;

  @Input()
  public videoLink: string;

  constructor(public videoservice: VideoService) {
  }

  ngOnInit(): void {
    this.videoLink = '';
  }

  // Add a video in the localStorage
  public addVideo(videoLinkVideo: string): void {
    this.videoservice.addVideo(this.videoLink);
    this.videoLink = '';
  }
  */

}
