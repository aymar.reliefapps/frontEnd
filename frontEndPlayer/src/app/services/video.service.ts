import { Injectable, Input } from '@angular/core';
import { Observable, of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Video } from '../classes/video';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  public linkis : string;
  
  private apiUrl = 'https://localhost:5001/api/VideoItems';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

    // Get the link of the video in SearchBar component
    public getLink(){
      if(this.linkis != null){
        return this.linkis;
        }
        else{
          return "";
        }
  }

  // Set link
  public setLink(vidLink: string){
    this.linkis = vidLink;
  }

   /** POST: add a new Video to the server */
  addVideo(video: Video): Observable<Video> {
    this.linkis = video.link;

    return this.http.post<Video>(this.apiUrl, video, this.httpOptions).pipe(
      tap((newVideo: Video) => console.log(`added video w/ id=${newVideo.id}`)),
      catchError(this.handleError<Video>('addVideo'))
    );
  }

  /** DELETE: delete the video from the server */
  deleteVideo(video: Video | number): Observable<Video> {
    const id = typeof video === 'number' ? video : video.id;
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete<Video>(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted video id=${id}`)),
      catchError(this.handleError<Video>('deleteVideo'))
    );
  }

  /** GET videos from the server */
  getVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(this.apiUrl)
      .pipe(
        tap(_ => console.log('fetched videos')),
        catchError(this.handleError<Video[]>('getVideos', []))
      );
  }
  
  /** GET video by id. Will 404 if id not found */
  getVideo(id: number): Observable<Video> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Video>(url).pipe(
      tap(_ => console.log(`fetched video id=${id}`)),
      catchError(this.handleError<Video>(`getVideo id=${id}`))
    );
  }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /* ********************************* ALL THIS IS ABOUT LOCAL STORAGE OF JAVASCRIPT ***********************************

  private linkVideo: string;
  private nextId: number;

  constructor(private http: HttpClient) {
    
    let videos = this.getVideos();

    // Have the good number of element to add with the good index in history
    if(videos.length == 0){
      this.nextId = 0;
    }
    else{
      let maxId = videos[(videos.length-1)].id;
      this.nextId = maxId + 1;
    }
   }
   // Add a video to the history
   public addVideo(link: string){
      let video = new Video(this.nextId, link);
      let videos = this.getVideos();
      videos.push(video);

      this.setLocalStorageVideo(videos);

      this.nextId++;
      this.linkVideo = link; 
   }

   // Get the link of the video in SearchBar component
   public getLink(){
      if(this.linkVideo != null){
         return this.linkVideo;
        }
        else{
          return "";
        }
   }

   // Set link
   public setLink(vid: string){
     this.linkVideo = vid;
   }

   // Get thenumber of element of the Array in localStorage of history
   public getLength(){
    let videos = this.getVideos();
     return videos.length;
   }

   public getVideos(): Video[]{
      let localStorageItem = JSON.parse(localStorage.getItem('video'));
      return localStorageItem == null ? [] : localStorageItem.videos;
   }

   public getVideo(id: number){
      let videos = this.getVideos();
      videos = videos.filter((video) => video.id == id);

      return videos == null ? Video : videos;
   }

   // Renvoyer la derniere video du localStorage of history
   public getLastVideo(){
    let videos = this.getVideos();

    if(videos.length != 0){
      return videos[videos.length-1].link;
    }
    else{
      return "";
    }
    
   }

   // Write the item in the localStorage when it is added in the array of history
   private setLocalStorageVideo(videos: Video[]): void{
      localStorage.setItem('video', JSON.stringify({ videos: videos }));
   }
   
   // Remove a video in localStorage of history
   public removeVideo(id: number): void {
      let videos = this.getVideos();
      videos = videos.filter((video) => video.id !== id);
      this.setLocalStorageVideo(videos);
    }

    */
  }
